Categories:Internet
License:Apache2
Web Site:https://duck.co/help/mobile/android
Source Code:https://github.com/duckduckgo/android
Issue Tracker:https://github.com/duckduckgo/android/issues

Auto Name:DuckDuckGo
Summary:Search widget
Description:
Search the web via duckduckgo.com, a search engine that's focussed
on privacy.

The app also works like a news reader, showing popular stories from a
customizable list of websites, until you enter a query.

Works with [[org.torproject.android]] (though not on Android 4.4).
.

Repo Type:git
Repo:https://github.com/duckduckgo/android.git

Build:2.1.1,52
    commit=v2.1.1
    srclibs=1:NetCipher@4fe34ede3f44d968e55

Build:2.1.2,53
    commit=v2.1.2
    submodules=yes

Build:2.1.3,54
    commit=v2.1.3
    submodules=yes

Build:2.1.4,55
    commit=v2.1.4
    submodules=yes

Build:2.1.6,57
    commit=v2.1.6
    submodules=yes
    prebuild=cp libs/android-support-v4.jar libs/OnionKit/libnetcipher/libs/android-support-v4.jar

Build:2.1.9,60
    commit=683ce1862af5aaa59bb4368ceae46312c57b64ef
    submodules=yes
    prebuild=cp libs/android-support-v4.jar libs/OnionKit/libnetcipher/libs/android-support-v4.jar

Build:2.1.10,61
    commit=c60349d8eacc02dc612ccc137d55816f66b8a876
    submodules=yes
    prebuild=cp libs/android-support-v4.jar libs/OnionKit/libnetcipher/libs/android-support-v4.jar

Maintainer Notes:
* Reset to UCM:Tags when https://github.com/duckduckgo/android/issues/145 is solved.
* Remove jar files.
* Maybe qualifies for AUM afterwards?
.

Auto Update Mode:None
#Update Check Mode:Tags
Update Check Mode:RepoManifest
Current Version:2.1.10
Current Version Code:61

